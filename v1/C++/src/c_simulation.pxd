from libcpp.vector cimport vector

cdef extern from "simulation.cpp":
    pass

cdef extern from "simulation.hpp":
    cdef cppclass Simulation:
        Simulation(int, double, double, int, double, int, double) except +
        int N_sites, T
        vector[double] GetTrajectory()
        void Run()
