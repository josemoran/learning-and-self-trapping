#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <vector>
#include <list>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <random>
#include <limits>
#include <set>
#include "simulation.hpp"


using namespace std;

int main(int argc, char const *argv[])
{
    int N_sites = 100;
    double gamma = 1.5;
    double beta = 2;
    int T = 200;
    double k = 1;
    
    int starting_site = 0;
    double alpha = 1;
    
    printf("Initializing simulation \n");
    cout << "N_sites = " << N_sites << endl;
    cout << "gamma = " << gamma << endl;
    cout << "beta = " << beta << endl;
    cout << "T = " << T << endl;
    cout << "k = " << k << endl;
    cout << "alpha = " << alpha << endl;

    Simulation sim(N_sites, gamma, beta, T, k, starting_site, alpha);
    cout << "-1 % 10 " << -1%10 << endl;
    sim.OneDRun();
    // for (int i = 0; i < T; ++i){
    //     sim.OneDStep();
    //     sim.UpdateTrajectory(i);
    // }
    // for (int i = 0; i < T; ++i)
    // {
    //     cout << sim.rand_us[i] << " ,";
    // }
    // cout << endl;
    // printf("Done\n");

    // printf("Running\n");

    // for (int i = 0; i < T; ++i)
    // {
    //     cout << "STEP" << i;
    //     sim.Step();
    // }
    // printf("Done, printing\n");
    sim.PrintTrajectories("trajectory.txt");
    return 0;
}
