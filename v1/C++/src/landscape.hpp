#pragma once

#include "memory_kernel.hpp"
#include <vector>


class Landscape
{
public:
  Landscape(int N_sites_, double beta_, MemoryKernel& kernel_, double k_, int starting_site_,  std::vector<double> utilities_, double alpha_);

  Landscape();
  std::vector<double> utilities;
  int position;
  int N_sites;
  double beta, alpha, k;
  std::vector< std::vector<double> > visited;

  MemoryKernel& kernel;

  void ForcePosition(int pos);

  double Attempt(int new_site);

  void UpdateVisited();
  void Accept(int new_site);
  double EffectiveUtility(int site);
  
};