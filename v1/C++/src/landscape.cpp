#include "landscape.hpp"
#include "memory_kernel.hpp"
#include <cmath>
#include <vector>
#include <iostream>

using namespace std;

Landscape::Landscape(int _N_sites, double _beta, MemoryKernel& _kernel, double _k, int starting_site,  std::vector<double> utilities_, double _alpha): N_sites(_N_sites), beta(_beta), kernel(_kernel), k(_k), alpha(_alpha), position(starting_site), utilities(utilities_){

  
  visited = vector<vector<double>>(N_sites, vector<double>(kernel.N, 0));
}

// Landscape::Landscape(){
//   utilities = std::vector<double>(10,0);
//   position = 0;
//   N_sites = 10;
//   beta = 1;
//   alpha = 1;
//   k = 1;
//   MemoryKernel& kernel(1,1,1.5);
//   visited = std::vector<std::vector<double> > (10, std::vector<double>(10,0));
// }

double Landscape::Attempt(int new_site){
  // printf("Attempting\n");
  double u_old = EffectiveUtility(position);
  // printf("Computed effective utility of current site\n");
  double u_new = alpha * utilities[new_site];
  if (visited[new_site][0] > 0) 
  {
    u_new = EffectiveUtility(new_site);
  }
  return exp(beta * (u_new - u_old));
}

void Landscape::Accept(int new_site){
  position = new_site;
}

void Landscape::UpdateVisited(){
  for (int i = 0; i < kernel.N; ++i)
  {
    visited[position][i] += 1;
    for (int j = 0; j < N_sites; ++j)
    {
      visited[j][i] *= kernel.exponentials[i];
    }
  }
}

double Landscape::EffectiveUtility(int site){
  // printf("check 0\n");
  double utility = utilities[site];
  // printf("check 1\n");
  double sum = 0;
  for (int i = 0; i < kernel.N; ++i)
  {
    // printf("check 2\n");
    sum += kernel.weights[i] * visited[site][i];
    // printf("check 3\n");
  }
  return utility * (1 + k*sum);
}


void Landscape::ForcePosition(int pos){
  position = pos;
}