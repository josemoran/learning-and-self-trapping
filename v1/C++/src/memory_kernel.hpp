#pragma once
#include <vector>


class MemoryKernel
{
public:
  MemoryKernel(int T, double gamma, double ratio);

  std::vector<double> weights; 
  std::vector<double> exponentials;
  int N;
  
  double Evaluate(double x);
};