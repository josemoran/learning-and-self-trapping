#include "simulation.hpp"
#include "memory_kernel.hpp"
#include "landscape.hpp"
#include <random>
#include <chrono> 


using namespace std;


Simulation::Simulation(int N_sites_, double gamma_, double beta_, int T_, double k_, int starting_site_, double alpha_){

    T = T_;

    kernel = new MemoryKernel(T, gamma_, 1.5);
    // T = T_;
    // N_sites = N_sites_;
    current_step = 0;
    N_sites = N_sites_;
    // fixed seed (useful for debugging)
    // unsigned seed = 2018;
    // random seed based on computer clock
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    generator = std::mt19937(seed);
    std::uniform_real_distribution<double> distribution(0.0,1.0);

    std::vector<double> utilities = Uniform_0_1(N_sites);


    system = new Landscape(N_sites_, beta_, *kernel, k_, starting_site_, utilities, alpha_);



    trajectories = std::vector<int>(T,0);
    trajectories[0] = starting_site_;

    for (int i = 0; i < T; ++i)
    {
        rand_us.push_back(distribution(generator));
    }

}


Simulation::~Simulation(){
    delete system;
    delete kernel;
}

vector<double> Simulation::Uniform_0_1(int N_sites){
    vector<double> random_array(N_sites,0);
    for (int i = 0; i < N_sites; ++i)
    {
        random_array[i] = distribution(generator);
    }
    return random_array;
}

void Simulation::Step(){
    int new_site = RandomSite();
    current_step += 1;
    if (system->Attempt(new_site) > rand_us[current_step]){
        system->Accept(new_site);
    }
    trajectories[current_step] = system->position;
    system->UpdateVisited();
}


void Simulation::OneDStep(){
    int step = distribution(generator) > 0.5 ? 1 : -1;
    int new_site = (system->position + step + N_sites) % N_sites;
    std::cout << "going to " << new_site << std::endl;
    current_step += 1;
    if (system->Attempt(new_site) > rand_us[current_step]){
        system->Accept(new_site);
    }
    trajectories[current_step] = system->position;
    system->UpdateVisited();
}

int Simulation::CurrentStep(){
    return current_step;
}

void Simulation::Run(){
    for (int i = 0; i < T; ++i)
    {
        int new_site = RandomSite();
        // std::cout << "Site " << new_site << "\n";
        if (system->Attempt(new_site) > rand_us[i])
        {
            // std::cout << "Accepted..." << endl;
            system->Accept(new_site);
            // std::cout << "OK"<<endl;
        }
        current_step +=1;
        system->UpdateVisited();
        trajectories[i] = system->position;
    }
}


void Simulation::OneDRun(){
    for (int i = 0; i < T; ++i)
    {
        int step = distribution(generator) > 0.5 ? 1 : -1;
        int new_site = ((system->position + step) % N_sites + N_sites) % N_sites;
        // std::cout << "Site " << new_site << "\n";
        if (system->Attempt(new_site) > rand_us[i])
        {
            // std::cout << "Accepted..." << endl;
            system->Accept(new_site);
            // std::cout << "OK"<<endl;
        }
        current_step +=1;
        system->UpdateVisited();
        trajectories[i] = system->position;
    }
}

int Simulation::RandomSite(){
    return int(distribution(generator)*N_sites);
}


void Simulation::PrintTrajectories(string txt){
    ofstream output_file(txt);
    for(int x : trajectories){
        output_file << x << "\n";
    }
}

vector<int> Simulation::GetTrajectory(){
    return trajectories;
}

void Simulation::SetUtility(int site, double value){
    system->utilities[site] = value;
}

int Simulation::AttemptAndGo(int site){
    double rand = distribution(generator);
    if (system->Attempt(site) > rand){
        system->Accept(site);
    }
    system->UpdateVisited();
    return system->position;
}




vector<int> Simulation::GetHistogram(){
    vector<int> histogram(N_sites,0);
    for (int t : trajectories){
        histogram[t] += 1;
    }
    return histogram;
}

void Simulation::PutTrajectory(int site, int time){
    trajectories[time] = site;
}

void Simulation::UpdateTrajectory(int time){
    trajectories[time] = system->position;
}


void Simulation::ConstantUtilities(){
    for (int i = 0; i < N_sites; ++i)
    {
        system->utilities[i] = 0.5;
    }
}

void Simulation::SetTrajectory(vector<int> dummy){
    trajectories = dummy;
}

int Simulation::GetPosition(){
    return system->position;
}

double Simulation::KernelEvaluate(double x){
    return kernel->Evaluate(x);
}

vector<double> Simulation::InitialUtility(){
    return system->utilities;
}

vector<double> Simulation::UpdatedUtility(){
    vector<double> utils;
    for (int i = 0; i < N_sites; ++i)
    {
        utils.push_back(system->EffectiveUtility(i));
    }
    return utils;
}


void Simulation::SetPosition(int pos){
    system->ForcePosition(pos % N_sites);
}

double Simulation::SiteUtility(){
    return system->EffectiveUtility(system->position);
}