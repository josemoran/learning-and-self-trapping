#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include "simulation.hpp"
#include <vector>


using namespace boost::python;

typedef std::vector<int> int_vector;

typedef std::vector<double> double_vector;


BOOST_PYTHON_MODULE(trapping)
{
  class_<int_vector>("int_vector")
  .def(vector_indexing_suite<int_vector>() );

  class_<double_vector>("double_vector")
  .def(vector_indexing_suite<double_vector>() );

  class_<Simulation>("simulation","A class for the learning and trapping simulation \n PARAMETERS : \n simulation(N_sites, gamma, beta, T, k, starting_site, alpha)"  ,init<int, double, double, int, double, int, double>())
  .def("get_trajectory", &Simulation::GetTrajectory)
  .def("run", &Simulation::Run)
  .def("constant_utilities", &Simulation::ConstantUtilities)
  .def("kernel_evaluate", &Simulation::KernelEvaluate)
  .def("set_utility", &Simulation::SetUtility)
  .def("attempt", &Simulation::AttemptAndGo)
  .def("step", &Simulation::Step)
  .def("set_trajectory", &Simulation::PutTrajectory)
  .def("update_trajectory", &Simulation::UpdateTrajectory)
  .def("current_step", &Simulation::CurrentStep)
  .def("utility", &Simulation::InitialUtility)
  .def("updated_utility", &Simulation::UpdatedUtility)
  .def("current_utility", &Simulation::SiteUtility)
  .def("oned_step", &Simulation::OneDStep)
  .def("oned_run", &Simulation::OneDRun)
  .def("get_histogram",&Simulation::GetHistogram)
  .def("set_position", &Simulation::SetPosition)
  .def("get_position", &Simulation::GetPosition);
}