#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <vector>
#include <list>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <random>
#include <limits>
#include <set>
#include "memory_kernel.hpp"
#include "landscape.hpp"


using namespace std; 
class Simulation
{
public:
    Simulation(int N_sites_, double gamma_, double beta_, int  T_, double k_, int starting_site_, double alpha_);

    ~Simulation();


    std::vector<int> trajectories;

    int T;

    int N_sites;
    std::mt19937 generator;
    std::uniform_real_distribution<double> distribution;
    Landscape* system; 
    MemoryKernel* kernel;
    std::vector<double> rand_us;
    int current_step;


    void Step();

    void Run();
    
    void PrintTrajectories(std::string filename);

    std::vector<double> Uniform_0_1(int N_sites);

    std::vector<int> GetTrajectory();

    void SetTrajectory(std::vector<int> dummy);

    int GetPosition();

    int CurrentStep();
    
    int RandomSite();

    void PutTrajectory(int site, int time);

    void UpdateTrajectory(int time);

    void SetUtility(int site, double value);

    int AttemptAndGo(int site);

    void ConstantUtilities();

    void SetPosition(int pos);

    std::vector<int> GetHistogram();

    double KernelEvaluate(double x);

    std::vector<double> InitialUtility();

    std::vector<double> UpdatedUtility();

    double SiteUtility();

    void OneDStep();

    void OneDRun();
};


