#include "memory_kernel.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

using namespace std;



MemoryKernel::MemoryKernel(int T, double gamma, double ratio = 1.5){
  // printf("Initializing Kernel\n");
  double total_time = ratio * T;
  N = int(log(1.7 * total_time));

  double beta = pow(total_time, 1/double(N));
  vector<double> c(N,1);
  double sum;
  // std::cout << "N=" << N << endl;
  for (int k = 1; k < N; ++k)
  {
    sum = 0;
    for (int i = 0; i < k-1; ++i)
    {
      sum += c[N-1-i] * pow(beta, -gamma*(k-i)) * exp(gamma * (1 - pow(beta, (i-k))));
    }
    c[N-1-k] = 1 - sum;
  }

  double weight;
  double exponential;

  for (int i = 0; i < N; ++i)
  {
    weight = c[i] * pow(beta, -gamma * i) * exp(-gamma * pow(beta, -i));
    exponential = exp(-gamma * pow(beta, -i));
    weights.push_back(weight);
    exponentials.push_back(exponential);
    
  }
  // printf("Done\n");
}

double MemoryKernel::Evaluate(double x){
  double sum = 0;
  for (int i = 0; i < N; ++i)
  {
    sum += weights[i] * pow(exponentials[i], x);
  }
  return sum;
}