import trapping
import numpy as np 
import matplotlib.pyplot as plt 


import matplotlib.animation as animation

N_sites = 50
gamma = 1.5
beta = 1.0
T = int(2e3)
k = 8
starting_site = 0
alpha = 1

sim1 = trapping.simulation(N_sites, gamma, beta, T, k, starting_site, alpha)
sim1.constant_utilities()



position = sim1.get_position()
initial_utilities = np.log(1+np.array(sim1.utility()))
new_utilities = np.log(1+np.array(sim1.updated_utility()))

positions = np.zeros(T)
time = np.arange(0, 1e-2*T, 1e-2)

positions[0] = position

traj_start_pos = 2

fig, ax = plt.subplots()
#trajectory being drawed
line4, = ax.step([position], [1], alpha=0.5, color='black')
#line between trajectory and ball
line5, = ax.plot([position,position],[new_utilities[position],2], color='black', alpha=0.5)
#effective and subjective utilities
line1, = ax.step(np.arange(N_sites)+0.5, new_utilities, color='red')
line2, = ax.step(np.arange(N_sites)+0.5, initial_utilities, color='blue')
#ball
line3, = ax.plot(position, new_utilities[position], 'ro', color='black', ms=5)
#line for the beginning of the trajectory
line6, = ax.plot([0,N_sites],[traj_start_pos,traj_start_pos],'--',color='black', lw=0.5)
ax.set_xlim(0,N_sites)
ax.set_ylim(0,5)


open('writer_test.mp4', 'w')

def animate(i):
    current_pos = sim1.get_position()
    jump_probs = [1/3.,1/3.,1/6.,1/6.]
    proposed_pos = np.random.choice([(current_pos+1), (current_pos-1), current_pos + 2, current_pos-2],p=jump_probs) % N_sites
    sim1.attempt(proposed_pos)
    position = sim1.get_position()
    positions[i] = position
    initial_utilities = np.log(1+np.array(sim1.utility()))
    new_utilities = np.log(1+np.array(sim1.updated_utility()))
    line1.set_data(np.arange(N_sites)+0.5, new_utilities)
    line3.set_data(position, new_utilities[position])
#     j = np.min(i,50)
    # j = min(i,50)
    line4.set_data(positions[:i],time[:i][::-1]+2)
    line5.set_data([position,position, positions[i-1]], [new_utilities[position], traj_start_pos, traj_start_pos])
    return [line1, line3, line4,line5]
    


FFMpegWriter = animation.writers['ffmpeg']
metadata = dict(title='Ca fonctionne !', artist='Matplotlib',
                comment='Movie support!')
writer = FFMpegWriter(fps=25, metadata=metadata)


# with writer.saving(fig, "writer_test.mp4", 100):
#     for i in range(T):
#         animate(i)
#         writer.grab_frame()
anim = animation.FuncAnimation(fig, animate, interval=5000, blit=True, frames=range(T))
anim.save('writer_test.mp4', writer=writer)