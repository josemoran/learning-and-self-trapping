import numpy as np 
import memory_kernel

# HELPER FUNCTION : default generator, uniform utility in [0;1]
uniform_0_1 = lambda s : np.random.uniform(0,1,size=s)



class landscape(object):
    '''A class for the utility-landscape with memory kernel. The graph structure is completely abstracted as it only
    enters in the way the new site is proposed. '''
    def __init__(self, N_sites, beta, kernel, k, starting_site = 0, random_generator = uniform_0_1, alpha = 1):
        '''Arguments : 
        N_sites : number of sites
        beta : temperature of the simulation
        kernel : memory kernel
        k : memory intensity
        random_generator : distribution for the utilities of the sites (can be correlated, for example)
        alpha : a-priori knowledge of the landscape'''


        self.x = starting_site
        self.k = k
        self.beta = beta
        self.alpha = alpha

        


        self.utilities = random_generator(N_sites)
        self.kernel = kernel
        self.visited = np.zeros((N_sites, self.kernel.N))
        self.ones = np.ones(self.kernel.N)

    def attempt(self, new_site):
        u_old = self.effective_utility(self.x)
        u_new = self.alpha * self.utilities[new_site]
        if (self.visited[new_site,0] > 0):
            u_new = self.effective_utility(new_site)
        return np.exp( self.beta* (u_new - u_old))

    def accept(self, new_site):
        self.x = new_site

    def update_visited(self):
        self.visited[self.x, :] += self.ones
        self.visited *= self.kernel.exponentials


    def effective_utility(self,site):
        utility = self.utilities[site]
        return utility * (1 + self.k * np.sum(self.kernel.weights * self.visited[site]))



