import numpy as np 
from memory_kernel import memory_kernel
from landscape import landscape
from landscape import uniform_0_1



def simulation(N_sites, gamma, beta, T, k, starting_site = 0, random_generator = uniform_0_1, alpha = 1):

    kernel = memory_kernel(gamma = gamma, T = T)
    system = landscape(N_sites = N_sites, beta = beta, kernel = kernel, k = k)

    trajectories = np.zeros(T)
    trajectories[0] = starting_site
    rand_us = np.random.rand(T)

    for i in xrange(1,T):
        new_site = np.random.randint(N_sites)
        if (system.attempt(new_site) > rand_us[i]):
            system.accept(new_site)
        system.update_visited()
        trajectories[i] = system.x

    return trajectories
