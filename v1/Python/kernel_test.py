import numpy as np 
from memory_kernel import memory_kernel as kernel
import matplotlib.pyplot as plt 



kernel = kernel(gamma =1.5, T = int(1e4), ratio=1.5)

x = np.arange(1,1e4)

y = [kernel.evaluate(xx) for xx in x]

plt.plot(x,y, label='sum of exponentials')
plt.plot(x, x**(-1.5), label='exact power-law')
plt.yscale('log')
plt.xscale('log')
plt.legend()
plt.xlabel('x')
plt.ylabel('f(x)')
plt.show()
