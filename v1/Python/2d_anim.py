import trapping
import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib.animation as animation



class TwoDLearning(object):
    
    def __init__(self,N,gamma,beta,T,k,starting_row, starting_column,alpha, constant_utilities=True):
        starting_site = N*starting_row + starting_column
        self.sim = trapping.simulation(N**2, gamma, beta, T, k, starting_site, alpha)
        if constant_utilities:
            self.sim.constant_utilities()
        self.N = N
        self.T = T
        self.propositions = np.random.choice(range(4), size=T)
        self.mvts = [(-1,0),(1,0),(0,-1),(0,1)]

    def propose_position(self,t):
        current = self.sim.get_position()
        column, row = self.return_position()
        step1, step2 = self.mvts[self.propositions[t]]
        
        column = (column + step1) % self.N
        row = (row + step2) % self.N
        
        new_position = row*self.N + column
        return new_position
        
    
    def step(self, t):        
        new_position = self.propose_position(t)
        self.sim.attempt(new_position)
        self.sim.update_trajectory(t)
    
    def return_trajectory(self):
        traj = np.array(self.sim.get_trajectory())
        return np.unravel_index(traj, (self.N, self.N))
    
    def return_position(self):
        pos = self.sim.get_position()
        column = pos % self.N 
        row = (pos - column) / self.N
        return column, row
    
    def run(self):
        for t in range(self.T):
            self.step(t)
        
    def get_utility(self):
        return np.array(self.sim.updated_utility()).reshape((self.N, self.N))
    



open('2d_animation.mp4', 'w')


N_sites = 100
gamma = 1.2
beta = 0.8
T = int(1e4)
k = 5.
starting_row = N_sites/2
starting_column = N_sites/2
alpha = 1

sim = TwoDLearning(N_sites, gamma, beta, T, k, starting_row, starting_column, alpha)
x,y = sim.return_position()


M = np.log(1+sim.get_utility())


    
fig, ax = plt.subplots()
matrice = ax.matshow(M)
plt.colorbar(matrice)
line = ax.plot(x,y, 'ro')[0]

def update(i):
    sim.step(i)
    matrice.set_array(np.log(1+sim.get_utility()))
    x,y = sim.return_position()
    line.set_data(x,y)
    


FFMpegWriter = animation.writers['ffmpeg']
metadata = dict(title='2D Learning', artist='JM',
                comment='Movie support!')
writer = FFMpegWriter(fps=25, metadata=metadata)

    
ani = animation.FuncAnimation(fig, update, frames=range(T), interval=50)
ani.save('2d_animation.mp4', writer=writer)