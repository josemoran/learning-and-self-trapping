import numpy as np 
import monte_carlo as mc 
import matplotlib.pyplot as plt

N_sites = 50
beta = 10 
k = 1
gamma = 1.5
T = int(1e5)

test_trajectories = mc.simulation(N_sites = N_sites, beta = beta, k = k, gamma = gamma, T = T)

np.savetxt('trajectory_example.txt', test_trajectories)

plt.plot(range(T), test_trajectories)
plt.title('N_sites = {}, beta = {}, k= {}, gamma = {}, T = {}'.format(N_sites, beta, k, gamma ,T))
plt.xlabel('time')
plt.ylabel('site')
plt.savefig('trajectory_example.png')