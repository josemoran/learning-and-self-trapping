import trapping
import numpy as np 
import matplotlib.pyplot as plt 


import matplotlib.animation as animation

N_sites = 50
gamma = 1.5
beta = 1.0
T = int(2e3)
k = 8
starting_site = 0
alpha = 1

sim1 = trapping.simulation(N_sites, gamma, beta, T, k, starting_site, alpha)
sim1.constant_utilities()

sim2 = trapping.simulation(N_sites, gamma, beta/2., T, k, starting_site, alpha)
sim2.constant_utilities()

sim3 = trapping.simulation(N_sites, gamma, beta * 2., T, k, starting_site, alpha)
sim3.constant_utilities()

sim4 = trapping.simulation(N_sites, gamma*2, beta * 2 , T, k, starting_site, alpha)
sim4.constant_utilities()

position1 = sim1.get_position()
initial_utilities1 = np.log(1+np.array(sim1.utility()))
new_utilities1 = np.log(1+np.array(sim1.updated_utility()))



position2 = sim2.get_position()
initial_utilities2 = np.log(1+np.array(sim2.utility()))
new_utilities2 = np.log(1+np.array(sim2.updated_utility()))


position3 = sim3.get_position()
initial_utilities3 = np.log(1+np.array(sim3.utility()))
new_utilities3 = np.log(1+np.array(sim3.updated_utility()))


position4 = sim4.get_position()
initial_utilities4 = np.log(1+np.array(sim4.utility()))
new_utilities4 = np.log(1+np.array(sim4.updated_utility()))



positions1 = np.zeros(T)

positions1[0] = position1

positions2 = np.zeros(T)

positions2[0] = position2

positions3 = np.zeros(T)

positions3[0] = position3

positions4 = np.zeros(T)

positions4[0] = position4



time = np.arange(0, 1e-2*T, 1e-2)



traj_start_pos = 2

fig, axs = plt.subplots(nrows = 2, ncols = 2)
#trajectory being drawed
line41, = axs[0,0].step([position1], [1], alpha=0.5, color='black')
#line between trajectory and ball
line51, = axs[0,0].plot([position1,position1],[new_utilities1[position1],2], color='black', alpha=0.5)
#effective and subjective utilities
line11, = axs[0,0].step(np.arange(N_sites)+0.5, new_utilities1, color='red')
line21, = axs[0,0].step(np.arange(N_sites)+0.5, initial_utilities1, color='blue')
#ball
line31, = axs[0,0].plot(position1, new_utilities1[position1], 'ro', color='black', ms=5)
#line for the beginning of the trajectory
line61, = axs[0,0].plot([0,N_sites],[traj_start_pos,traj_start_pos],'--',color='black', lw=0.5)
axs[0,0].set_xlim(0,N_sites)
axs[0,0].set_ylim(0,5)
axs[0,0].set_title('$\\beta$ = {}, $\\gamma$ = {}'.format(beta, gamma))


line42, = axs[0,1].step([position2], [1], alpha=0.5, color='black')
#line between trajectory and ball
line52, = axs[0,1].plot([position2,position2],[new_utilities2[position2],2], color='black', alpha=0.5)
#effective and subjective utilities
line12, = axs[0,1].step(np.arange(N_sites)+0.5, new_utilities2, color='red')
line22, = axs[0,1].step(np.arange(N_sites)+0.5, initial_utilities2, color='blue')
#ball
line32, = axs[0,1].plot(position2, new_utilities2[position2], 'ro', color='black', ms=5)
#line for the beginning of the trajectory
line62, = axs[0,1].plot([0,N_sites],[traj_start_pos,traj_start_pos],'--',color='black', lw=0.5)
axs[0,1].set_xlim(0,N_sites)
axs[0,1].set_ylim(0,5)
axs[0,1].set_title('$\\beta$ = {}, $\\gamma$ = {}'.format(beta/2., gamma))

line43, = axs[1,0].step([position3], [1], alpha=0.5, color='black')
#line between trajectory and ball
line53, = axs[1,0].plot([position3,position3],[new_utilities3[position3],2], color='black', alpha=0.5)
#effective and subjective utilities
line13, = axs[1,0].step(np.arange(N_sites)+0.5, new_utilities3, color='red')
line23, = axs[1,0].step(np.arange(N_sites)+0.5, initial_utilities3, color='blue')
#ball
line33, = axs[1,0].plot(position3, new_utilities3[position3], 'ro', color='black', ms=5)
#line for the beginning of the trajectory
line63, = axs[1,0].plot([0,N_sites],[traj_start_pos,traj_start_pos],'--',color='black', lw=0.5)
axs[1,0].set_xlim(0,N_sites)
axs[1,0].set_ylim(0,5)
axs[1,0].set_title('$\\beta$ = {}, $\\gamma$ = {}'.format(beta*2, gamma))


line44, = axs[1,1].step([position4], [1], alpha=0.5, color='black')
#line between trajectory and ball
line54, = axs[1,1].plot([position4,position4],[new_utilities4[position4],2], color='black', alpha=0.5)
#effective and subjective utilities
line14, = axs[1,1].step(np.arange(N_sites)+0.5, new_utilities4, color='red')
line24, = axs[1,1].step(np.arange(N_sites)+0.5, initial_utilities4, color='blue')
#ball
line34, = axs[1,1].plot(position4, new_utilities4[position4], 'ro', color='black', ms=5)
#line for the beginning of the trajectory
line64, = axs[1,1].plot([0,N_sites],[traj_start_pos,traj_start_pos],'--',color='black', lw=0.5)
axs[1,1].set_xlim(0,N_sites)
axs[1,1].set_ylim(0,5)
axs[1,1].set_title('$\\beta$ = {}, $\\gamma$ = {}'.format(beta *2, gamma*2))


open('comparisons.mp4', 'w')

def animate(i):
    jump_probs = [1/3.,1/3.,1/6.,1/6.]

    current_pos1 = sim1.get_position()
    proposed_pos1 = np.random.choice([(current_pos1+1), (current_pos1-1), current_pos1 + 2, current_pos1-2],p=jump_probs) % N_sites
    sim1.attempt(proposed_pos1)
    position1 = sim1.get_position()
    positions1[i] = position1
    initial_utilities1 = np.log(1+np.array(sim1.utility()))
    new_utilities1 = np.log(1+np.array(sim1.updated_utility()))
    line11.set_data(np.arange(N_sites)+0.5, new_utilities1)
    line31.set_data(position1, new_utilities1[position1])
#     j = np.min(i,50)
    # j = min(i,50)
    line41.set_data(positions1[:i],time[:i][::-1]+2)
    line51.set_data([position1,position1, positions1[i-1]], [new_utilities1[position1], traj_start_pos, traj_start_pos])

    current_pos2 = sim2.get_position()
    proposed_pos2 = np.random.choice([(current_pos2+1), (current_pos2-1), current_pos2 + 2, current_pos2-2],p=jump_probs) % N_sites
    sim2.attempt(proposed_pos2)
    position2 = sim2.get_position()
    positions2[i] = position2
    initial_utilities2 = np.log(1+np.array(sim2.utility()))
    new_utilities2 = np.log(1+np.array(sim2.updated_utility()))
    line12.set_data(np.arange(N_sites)+0.5, new_utilities2)
    line32.set_data(position2, new_utilities2[position2])
#     j = np.min(i,50)
    # j = min(i,50)
    line42.set_data(positions2[:i],time[:i][::-1]+2)
    line52.set_data([position2,position2, positions2[i-1]], [new_utilities2[position2], traj_start_pos, traj_start_pos])

    current_pos3 = sim3.get_position()
    proposed_pos3 = np.random.choice([(current_pos3+1), (current_pos3-1), current_pos3 + 2, current_pos3-2],p=jump_probs) % N_sites
    sim3.attempt(proposed_pos3)
    position3 = sim3.get_position()
    positions3[i] = position3
    initial_utilities3 = np.log(1+np.array(sim3.utility()))
    new_utilities3 = np.log(1+np.array(sim3.updated_utility()))
    line13.set_data(np.arange(N_sites)+0.5, new_utilities3)
    line33.set_data(position3, new_utilities3[position3])
#     j = np.min(i,50)
    # j = min(i,50)
    line43.set_data(positions3[:i],time[:i][::-1]+2)
    line53.set_data([position3,position3, positions3[i-1]], [new_utilities3[position3], traj_start_pos, traj_start_pos])

    current_pos4 = sim4.get_position()
    proposed_pos4 = np.random.choice([(current_pos4+1), (current_pos4-1), current_pos4 + 2, current_pos4-2],p=jump_probs) % N_sites
    sim4.attempt(proposed_pos4)
    position4 = sim4.get_position()
    positions4[i] = position4
    initial_utilities4 = np.log(1+np.array(sim4.utility()))
    new_utilities4 = np.log(1+np.array(sim4.updated_utility()))
    line14.set_data(np.arange(N_sites)+0.5, new_utilities4)
    line34.set_data(position4, new_utilities4[position4])
#     j = np.min(i,50)
    # j = min(i,50)
    line44.set_data(positions4[:i],time[:i][::-1]+2)
    line54.set_data([position4,position4, positions4[i-1]], [new_utilities4[position4], traj_start_pos, traj_start_pos])
    return [line11, line31, line41,line51, line12, line32, line42,line52, line13, line33, line43,line53, line14, line34, line44,line54 ]
    


FFMpegWriter = animation.writers['ffmpeg']
metadata = dict(title='Habit formation', artist='JM',
                comment='Movie support!')
writer = FFMpegWriter(fps=25, metadata=metadata)


# with writer.saving(fig, "comparisons.mp4", 100):
#     for i in range(T):
#         animate(i)
#         writer.grab_frame()
anim = animation.FuncAnimation(fig, animate, interval=8000, blit=True, frames=range(T))
anim.save('comparisons.mp4', writer=writer)