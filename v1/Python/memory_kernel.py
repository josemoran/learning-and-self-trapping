import numpy as np 


#HELPER FUNCTION : optimal time per cited article
def _optimal_N(time):
    return int(1.7 * np.log(time))


class memory_kernel(object):
    '''Implements a memory kernel from https://arxiv.org/pdf/physics/0605149.pdf'''

    def __init__(self, T, gamma, ratio = 1.5):
        '''A memory kernel using a sum of exponentials to imitate a power-law decay.
        Parameters: 
        T: (time-scale for which the kernel will be used), 
        ratio (>1): (ratio between the time-scale and the maximal time-scale of the sum, 1.5 seems to work quite well) 
        gamma: power-law exponent 

        Represents a function f(x)=1/x^gamma as g(x)= sum_i w_i exp(lambda_i x)
        '''

        total_time = ratio*T
        N = _optimal_N(total_time)
        beta = total_time ** (1/float(N))

        self.c = np.ones(N)
        self.N = N
        
        for k in xrange(1,N):
            _sum = 0
            for i in xrange(0,k-1):
                _sum += self.c[N-1-i] * beta**(-gamma * (k-i)) * np.exp(gamma * (1 - beta**(i-k)))
            self.c[N-1-k] = 1 - _sum

        self.weights = self.c * beta**(-gamma * np.arange(N))*np.exp( -gamma * beta**(-np.arange(N)))
        self.exponentials = np.exp( -gamma * beta**(-np.arange(N)))

        


    def evaluate(self,x):
        return np.sum(self.weights*self.exponentials**x)
