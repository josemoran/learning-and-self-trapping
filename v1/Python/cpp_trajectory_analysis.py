import numpy as np 
import matplotlib.pyplot as plt 



trajectory = np.loadtxt('trajectorycpp.txt')

#array containing True if the particle moved
changes = (np.diff(trajectory)!=0)

#array containing the time-steps between changes
trapping_times = np.diff(np.where(changes)[0])


plt.hist(trapping_times, bins=500)
plt.yscale('log')
plt.xlabel('Trapping times')
plt.ylabel('Frequency')
plt.savefig('trapping_times_histo.png')