import numpy as np 


class fishing_ponds(object):

    def __init__(self, M, N, nu, epsilon, K):
        self.M = M
        self.N = N
        self.nu = nu
        self.epsilon = epsilon
        self.K = K

        self.fish_population = (0.75+0.25*np.random.rand(M))*K

        self.fishermen = np.random.randint(M, size=N)

        self.fishing_loads = np.zeros(M)


    def step(self):
        proposal = np.random.randint(self.M, size=self.N)
        moves = (self.fish_population[proposal]>self.fish_population[self.fishermen])




        sites, loads = np.unique(self.fishermen, return_counts = True)
        self.fish_population *= 1+self.nu*(1-self.fish_population/self.K)
        self.fish_population[sites] *= 1 - loads*self.epsilon
        self.fish_population *= (self.fish_population>1e-4)

        self.fishermen = moves*proposal+(1-moves)*self.fishermen
        
